///<reference types="cypress" />;
import RegisterPage from "../../support/page_object_model/registerPage";

const registerPage = new RegisterPage();


describe('Main Page Elements and their main functionality  for Register Page', ()=>{

it('Precondition, visit register page', ()=>{
    cy.visit('/user/register');
   
});

it('Header Create an Account', ()=>{
    registerPage.headerCreateAnAccount().should('be.visible').should('have.text', "Create an account");
});
it('Input First Name contains "First Name"', ()=>{
    registerPage.checkNameOfFields(registerPage.pageElem.commonEl.inputField.firstName);
});
it('Input Last Name constains "Last name"', ()=>{
    registerPage.checkNameOfFields(registerPage.pageElem.commonEl.inputField.lastName);
});
it('Email input constains "Email"', ()=>{
    registerPage.checkNameOfFields(registerPage.pageElem.commonEl.inputField.email);
});
it('Password input constains "Password"', ()=>{
    registerPage.checkNameOfFields(registerPage.pageElem.commonEl.inputField.password);
});
it('Register button exists', ()=>{
    registerPage.submitBtn().should('be.visible').should('have.text', 'Register');
});
it('Text under register button', ()=>{
    registerPage.textUnderRegisterBtn().should('be.visible').should('have.text', "Already have an account? Just click Log in.");
});
it('Link LogIn is visable', ()=>{
    registerPage.linkLogIn().should('have.text', "Log in");
    
});
it('Link LogIn is clicable and redirects to a new page', ()=>{
    registerPage.linkLogIn().click();
    registerPage.headerAfterLinkLogIn().should('be.visible').should('have.text', "Welcome back!")
    cy.location('pathname').should('eq', '/v3/user/login').go('back');
});
})

   
