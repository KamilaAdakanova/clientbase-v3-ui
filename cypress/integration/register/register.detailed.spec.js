import RegisterPage from "../../support/page_object_model/registerPage";
import Helper from "../../support/helper";

const registerPage = new RegisterPage();
const helper = new Helper()


describe('Detailed Tests for Register Page', ()=>{

it('Precondition, visit register page', ()=>{
    cy.visit('/user/register');
});
it('Check Input Email with lower Case Latin letters ', ()=>{
    helper.checkDetailTestsForInputFields(registerPage.pageElem.commonEl.inputField.email)
});
});