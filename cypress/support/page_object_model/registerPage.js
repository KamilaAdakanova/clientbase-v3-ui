
export default class RegisterPage{

   // selectors
headerCreateAnAccount =()=> cy.get('form#user_login').children('h1')
firstNameField =()=> cy.get('input#user_login_firstName')
lastNameField  =()=> cy.get('input#user_login_lastName')
emailField  =()=> cy.get('input#user_login_email')
passwordField =()=> cy.get('input#user_login_password')
submitBtn =()=> cy.get('button[type="submit"]')
headerConfirmEmail =()=> cy.get('span[class="ms-2 fw-bold"]')
headerError =()=> cy.get('div[class=ant-notification-notice-message]')
textUnderRegisterBtn =()=> cy.get('[href="/v3/user/login"]').parent('p')
linkLogIn =()=> cy.get('[href="/v3/user/login"]')
headerAfterLinkLogIn =()=> cy.get('h1')


//methods
randomEmail=()=>{return (`newuser${Math.floor(Math.random() * 1000)}@gmail.com`);
}
 randomPassword=()=>{return (`newuser${Math.floor(Math.random() * 1000)}`);
 }

 checkNameOfFields = (nameObject ) => {
  cy.get(nameObject.selectorRegister)
  .should('be.visible')
    .should('have.attr', 'placeholder', nameObject.placeHolder)
    .type('abc123QAZ!@#')
    .should('have.value', 'abc123QAZ!@#')
  .type(
    '{backspace}{leftArrow}{rightArrow}{del}{leftArrow}{leftArrow}{backspace}{leftArrow}{leftArrow}{backspace}')
    .type('{selectAll}{del}').should('have.value', '');
    
};
 

    pageElem = {
        commonEl: {
          inputField: {
            firstName: {
                selectorRegister: 'input#user_login_firstName',
                placeHolder: 'First Name',
            },
            lastName: {
                selectorRegister: 'input#user_login_lastName',
                placeHolder: 'Last Name',
            },

            email: {
              selectorRegister: 'input#user_login_email',
              //selectorLogin: 'input#normal_login_email',
              placeHolder: 'Email',
            },
            password: {
              selectorRegister: 'input#user_login_password',
              //selectorLogin: 'input#normal_login_password',
              placeHolder: 'Password',
            },
        }
    }
}
checkNameOfFields = (nameObject) => {
    cy.get(nameObject.selectorRegister)
    .should('be.visible')
      .should('have.attr', 'placeholder', nameObject.placeHolder)
      .type('abc123QAZ!@#')
      .should('have.value', 'abc123QAZ!@#')
    .type(
      '{backspace}{leftArrow}{rightArrow}{del}{leftArrow}{leftArrow}{backspace}{leftArrow}{leftArrow}{backspace}')
      .type('{selectAll}{del}').should('have.value', '');
      
  };


}