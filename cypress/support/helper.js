

export default class Helper{

    positive = {
        'lowerCaseLatinLetters': "abcdefghijklmopqrstuvwxyz",  
        'upperCaseLatinLetters': "ABCDEFGHIJKLMOPQRSTUVWXYZ",
        
        '2symbols': "Ed",
        '100symbols': "mVVIgWZktcsMbVIrfALHyRuqsELShOIOIiqAMvQsOCByTwUPQRLvlvllDYaFXhOxPRwXPhjIoasCZOvVpFCMLqIBlBXfmykRAdwa",
        'doubleNamesWith1Space': "Sarah Blanche",
    }

    negative ={
        'accepts-': "My-name",
    }

    checkDetailTestsForInputFirstAndLastFields = (nameObject) => {
        cy.get(nameObject.selectorRegister)
        .should('be.visible')
          .should('have.attr', 'placeholder', nameObject.placeHolder)
          .type(this.positive.lowerCaseLatinLetters)
          .should('have.value', this.positive.lowerCaseLatinLetters)
          .clear()
          .type(this.positive.upperCaseLatinLetters)
          .should('have.value', this.positive.upperCaseLatinLetters)
          .clear()
          .type(this.positive)
          .should('have.value', this.positive)

    }



}